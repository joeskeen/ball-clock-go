package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	args := os.Args[1:]
	if len(args) < 1 || len(args) > 2 {
		panic("Invalid number of parameters. Must pass one or two numbers.")
	}
	balls, err1 := strconv.ParseInt(args[0], 10, 32)
	if err1 != nil {
		panic("Arguments must be numeric.")
	}

	if len(args) == 1 {
		days := DaysToReset(int(balls))
		fmt.Printf("%s balls cycle after %s days.",
			strconv.FormatInt(balls, 10),
			strconv.FormatInt(int64(days), 10))
	} else {
		minutes, err2 := strconv.ParseInt(args[1], 10, 8)
		if err2 != nil {
			panic("Arguments must be numeric.")
		}
		status := StatusAfter(int(balls), int(minutes))
		data, _ := json.Marshal(status)
		jsonString := strings.Replace(string(data), "null", "[]", -1)
		fmt.Print(jsonString)
	}
}
