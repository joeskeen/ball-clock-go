package main

import "testing"

func TestTick(t *testing.T) {
	clock, err := NewBallClock(27)
	if err != "" {
		t.Error(err)
	}

	for i := 1; i <= 1000000; i++ {
		clock.Tick()
	}

}

func TestTooSmallCapacity(t *testing.T) {
	_, err := NewBallClock(26)
	if err == "" {
		t.Errorf("Expected error for capacity of 26, but got %q", err)
	}
}

func TestTooLargeCapacity(t *testing.T) {
	_, err := NewBallClock(128)
	if err == "" {
		t.Errorf("Expected error for capacity of 128, but got %q", err)
	}
}

func TestIsResetAtStart(t *testing.T) {
	clock, _ := NewBallClock(45)
	if !clock.IsReset() {
		t.Errorf("A new clock should be reset: %q", clock.MainQueue)
	}
}

func TestIsNotResetAfterOneIteration(t *testing.T) {
	clock, _ := NewBallClock(45)
	clock.Tick()
	if clock.IsReset() {
		t.Errorf("A clock after one iteration should not be reset: %q", clock.MainQueue)
	}
}
