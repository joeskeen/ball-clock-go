package main

import "encoding/json"

type BallClock struct {
	Capacity        int
	MinuteTrack     *Track
	FiveMinuteTrack *Track
	HourTrack       *Track
	MainQueue       []int
}

func NewBallClock(capacity int) (*BallClock, string) {
	clock := &BallClock{
		Capacity:        capacity,
		MinuteTrack:     NewTrack(4),
		FiveMinuteTrack: NewTrack(11),
		HourTrack:       NewTrack(11),
	}
	if capacity < 27 || capacity > 127 {
		return nil, "Capacity must be between 27 and 127."
	}

	for i := 1; i <= capacity; i++ {
		clock.MainQueue = append(clock.MainQueue, i)
	}

	return clock, ""
}

func (c *BallClock) Tick() {
	ball, queue := c.MainQueue[0], c.MainQueue[1:]
	tracks := []*Track{c.MinuteTrack, c.FiveMinuteTrack, c.HourTrack}
	passOn := ball
	var enqueue []int
	for i := 0; passOn > 0 && i < len(tracks); i++ {
		passOn, enqueue = tracks[i].AddToTrack(ball)
		if len(enqueue) > 0 {
			queue = append(queue, enqueue...)
		}
	}
	if passOn > 0 {
		queue = append(queue, passOn)
	}
	c.MainQueue = queue
}

func (c *BallClock) IsReset() bool {
	if len(c.MainQueue) != c.Capacity {
		return false
	}
	for i := 1; i <= c.Capacity; i++ {
		if c.MainQueue[i-1] != i {
			return false
		}
	}
	return true
}

type ClockStatus struct {
	Min     []int
	FiveMin []int
	Hour    []int
	Main    []int
}

func (c *BallClock) GetStatus() ClockStatus {
	return ClockStatus{
		c.MinuteTrack.Balls,
		c.FiveMinuteTrack.Balls,
		c.HourTrack.Balls,
		c.MainQueue,
	}
}

func (c BallClock) String() string {
	status := c.GetStatus()
	data, _ := json.Marshal(status)
	return string(data)
}
