package main

type Track struct {
	Balls    []int
	Capacity int
}

func NewTrack(capacity int) *Track {
	return &Track{Capacity: capacity}
}

func (t *Track) AddToTrack(ballNumber int) (passOn int, enqueue []int) {
	if len(t.Balls) < t.Capacity {
		t.Balls = append(t.Balls, ballNumber)
	} else {
		enqueue = reverse(t.Balls)
		passOn = ballNumber
		t.Balls = []int{}
	}
	return
}

func reverse(r []int) []int {
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	return r
}
