package main

import "math"

func DaysToReset(balls int) int {
	clock, _ := NewBallClock(balls)
	clock.Tick()
	i := 1
	for ; !clock.IsReset(); i++ {
		clock.Tick()
	}
	minutesInADay := float64(60 * 24)
	days := int(math.Floor(float64(i) / minutesInADay))
	return days
}

func StatusAfter(balls int, minutes int) ClockStatus {
	clock, _ := NewBallClock(balls)
	for i := 0; i < minutes; i++ {
		clock.Tick()
	}
	return clock.GetStatus()
}
