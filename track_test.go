package main

import "testing"

func TestAddToTrack(t *testing.T) {
	track := Track{Capacity: 3}
	for i := 1; i <= 3; i++ {
		passOn, _ := track.AddToTrack(i)
		if passOn > 0 {
			t.Errorf("Track overflowed after %q balls", i)
		}
	}
	passOn, enqueue := track.AddToTrack(4)
	if passOn != 4 {
		t.Error("Track did not overflow on fourth ball")
	}
	queue := []int{3, 2, 1}
	for i, want := range queue {
		got := enqueue[i]
		if got != want {
			t.Errorf("Expected enqueue output at index %q to be %q, but got %q", i, want, got)
		}
	}
}
