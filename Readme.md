# `ballclock`

A Go language implementation of the `ball-clock-node` repository <https://bitbucket.org/joeskeen/ball-clock-node>.

## Notes

I had a hard time understanding how errors are handled in Go.  Unlike other languages I have worked with,
Go has no throw statement, or built-in Error/Exception type.  When I was finishing up this project, implementation
realized that there is another way I could handle errors using the `error` interface.  If I get the chance,
I will go back and fix this.
