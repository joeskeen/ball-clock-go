package main

import "testing"

func Test30Days(t *testing.T) {
	balls := 30
	expected := 15
	result := DaysToReset(balls)
	if expected != result {
		t.Errorf("Expected %q balls to reset after %q days, but got %q.", balls, expected, result)
	}
}

func Test45Days(t *testing.T) {
	balls := 45
	expected := 378
	result := DaysToReset(balls)
	if expected != result {
		t.Errorf("Expected %q balls to reset after %q days, but got %q.", balls, expected, result)
	}
}

func TestStatus(t *testing.T) {
	balls := 30
	minutes := 325
	expectedMin := []int{}
	expectedFiveMin := []int{22, 13, 25, 3, 7}
	expectedHour := []int{6, 12, 17, 4, 15}
	expectedMain := []int{11, 5, 26, 18, 2, 30, 19, 8, 24, 10, 29, 20, 16, 21, 28, 1, 23, 14, 27, 9}
	result := StatusAfter(balls, minutes)

	if len(result.Min) != len(expectedMin) {
		t.Errorf("Expected minute to have %q balls, but instead found %q.", len(result.Min), len(expectedMin))
	}
	for i, want := range expectedMin {
		if result.Min[i] != want {
			t.Errorf("Expected minute track position %q to have ball #%q, but instead found %q.", i, want, result.Min[i])
		}
	}

	if len(result.FiveMin) != len(expectedFiveMin) {
		t.Errorf("Expected five minute to have %q balls, but instead found %q.", len(result.FiveMin), len(expectedFiveMin))
	}
	for i, want := range expectedFiveMin {
		if result.FiveMin[i] != want {
			t.Errorf("Expected five minute track position %q to have ball #%q, but instead found %q.", i, want, result.FiveMin[i])
		}
	}

	if len(result.Hour) != len(expectedHour) {
		t.Errorf("Expected hour to have %q balls, but instead found %q.", len(result.Hour), len(expectedHour))
	}
	for i, want := range expectedHour {
		if result.Hour[i] != want {
			t.Errorf("Expected hour track position %q to have ball #%q, but instead found %q.", i, want, result.Hour[i])
		}
	}

	if len(result.Main) != len(expectedMain) {
		t.Errorf("Expected main to have %q balls, but instead found %q.", len(result.Main), len(expectedMain))
	}
	for i, want := range expectedMain {
		if result.Main[i] != want {
			t.Errorf("Expected main track position %q to have ball #%q, but instead found %q.", i, want, result.Main[i])
		}
	}
}
